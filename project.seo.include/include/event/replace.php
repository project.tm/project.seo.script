<?

namespace Project\Seo\Script\Event;

use Project\Seo\Script\Data;

include_once (__DIR__ . '/base/event.php');

class Replace extends Base\Event {

    static public function paser(&$arResult, $arData) {
        if (count($arData) < 3) {
            return;
        }
        $arData = array(
            'SEARCH' => $arData[0],
            'REPLACE' => $arData[1],
        );
        if (empty($arData['SEARCH']) or empty($arData['REPLACE'])) {
            return;
        }
        $arResult[$arData['SEARCH']] = $arData['REPLACE'];
    }

    static public function OnEndBufferContent(&$content) {
        $arResult = Data::get('text');
        $content = str_replace(array_keys($arResult), $arResult, $content);
    }

}
